# We are excited you are taking the time to solve our technical assignment! #

### Let’s pretend you are starting your first day with us. After you settle in, your first task will be assigned. Both Joseph (Product Owner) and Ness (Technical Lead) are ready to give you more details on your first task. ###

* Joseph - “Hi, I have your first task, excited?

* We need to run an application in our brand new Kubernetes cluster and we don't know how we could do that”

* Ness - “Ok, I understand the request and I think I can provide some help. We can combine some modern technologies to package, deploy and run our applications in a scalable platform making sure it eases the engineers day-to-day work.

## Approach

Leveraging Helm and Terraform would be an excellent idea.  
There are definitely benefits to it but feel free to use other technologies/frameworks (ie.: flux). Obviously if you want to use other tools/technologies to help you achieve the work, I’m happy with it, but remember the focus must be as much in having it working as it should having flexibility (ie.: target different Kubernetes clusters).  
Bonus points if you are able to explain the reason why you used certain components. It is important engineers that will adopt the tooling and methodology are able to understand them correctly.

## Premisses

Its a given a Kubernetes cluster is already setup for whoever leverages this work to run the application. For you purposes, you can manually setup a `Kind` cluster in you local development environment. `minikube` or `k3d` would work just fine as well.

Assume you have a docker image already available as starting point. Its out of scope for this exercise to write an application or even a Dockerfile for it.
You can use any publicly available image (ie.: https://hub.docker.com/_/nginx).

## Goals

It is in the scope of this exercise to write the Helm chart for the application as well as setting up the mechanics to deploy it to an existing Kubernetes cluster.
After its deployed, one must be able to interact with it - from a network outside the cluster, hit the webpage or API exposed by the application.


## Submit your work

Once you are done please commit the code and create a Pull Request so we can code review it.

Now that the exercise has been explained, you can start working on it, we normally request for it to be uploaded into our git repository within 3 working days, but can be changed if you need more time to start. This task should take you no longer than 3 hours and needless to say should be totally completed by you, after all this is supposed to be a fun challenge! 

## We will provide you a git repository in bitbucket for the code to be uploaded. Please see the instructions below:

Please sign up for an account on Bitbucket if you don't have one already. If you do, feel free to use your own login / profile.

If you're not familiar with GIT, use the Sourcetree client to get started or visit http://git-scm.com for the official git client.

Once you're done with your work, COMMIT and then PUSH (ie. send to bitbucket). The PUSH with notify us and we will review your work. Only push when you're done, and push once. This is to avoid us reviewing an incomplete submission.


### We're here to help...
Should you run into problems or have any questions - please get in touch with either your recruitment agent or with Renato Oliveira - renato.oliveira@craftablesoftware.com
* Good luck